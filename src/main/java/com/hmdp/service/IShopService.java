package com.hmdp.service;

import com.hmdp.entity.Shop;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 商铺 服务接口
 */
public interface IShopService extends IService<Shop> {

    Object queryById(Long id);

    Object update(Shop shop);
}
