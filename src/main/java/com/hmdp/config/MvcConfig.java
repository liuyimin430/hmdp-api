package com.hmdp.config;

import com.hmdp.utils.LoginInterceptor;
import com.hmdp.utils.RefreshTokenInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * @Date: 2022/9/14
 * @Version: 1.0
 * @Author: YiMin
 * @Description: 拦截器配置类
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

    // 这里注入stringRedisTemplate对象，放在拦截器中当做构造方法引入使用
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 实现添加拦截器方法
     *
     * @param registry (拦截器的注册器对象)
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 登录拦截器(拦截部分请求)
        registry.addInterceptor(new LoginInterceptor())
                .excludePathPatterns(
                        "/shop/**",
                        "/voucher/**",
                        "/shop-type/**",
                        "/upload/**",
                        "/blog/hot",
                        "/user/code",
                        "/user/login"
                ).order(1);

        // token刷新的拦截器(默认拦截所有请求)
        registry.addInterceptor(new RefreshTokenInterceptor(stringRedisTemplate)).addPathPatterns("/**").order(0);
    }
}
